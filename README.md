# TabTracker
This app for song tracking. Reliable CRUD functionality. Only authorized people access the crud functionality. 
TabTracker that can help you to bookmark you song. You can also bookmark youtube video.
TabTracker always track you. Whatever you saw. Nice pagination that help you easily access all history.

# Server Side Technology
1. Nodejs
2. Express Server

# Client Side Technology
1. Vuejs 
2. axios (gor http request)
3. Vuex

# Online Access
https://tranquil-everglades-10100.herokuapp.com 
