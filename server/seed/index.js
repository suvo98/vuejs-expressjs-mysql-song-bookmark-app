const {
  sequelize,
  Song,
  User,
  Bookmarks,
  History
} = require('../src/models')

const Promise = require('bluebird')
const songs = require('./songs.json')
const users = require('./users.json')
const bookmarks = require('./bookmarks.json')
const songsHistory = require('./songsHistory.json')

sequelize.sync({force: true})
  .then(async function () {
    await Promise.all(
      users.map(user => {
        User.create(user)
      })
    )

    await Promise.all(
      songs.map(song => {
        Song.create(song)
      })
    )

    await Promise.all(
      bookmarks.map(bookmarks => {
        Bookmarks.create(bookmarks)
      })
    )

    await Promise.all(
      songsHistory.map(songsHistory => {
        History.create(songsHistory)
      })
    )
  })
