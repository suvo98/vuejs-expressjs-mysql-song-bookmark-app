module.exports = (sequelize, DataTypes) => {
  const Bookmarks = sequelize.define('Bookmarks', {})

  Bookmarks.associate = function (models) {
    Bookmarks.belongsTo(models.User)
    Bookmarks.belongsTo(models.Song)
  }

  return Bookmarks
}
