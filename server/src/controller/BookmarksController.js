const {
  Bookmarks,
  Song
} = require('../models')
const _ = require('lodash')

module.exports = {
  async index (req, res) {
    try {
      const userId = req.user.id
      const {songId} = req.query

      const where = {
        UserId: userId
      }
      if (songId) {
        where.SongId = songId
      }
      const bookmarks = await Bookmarks.findAll({
        where: where,
        include: [
          {
            model: Song
          }
        ]
      })
        .map(bookmark => bookmark.toJSON())
        .map(bookmark => _.extend(
          {},
          bookmark.Song,
          bookmark
        ))

      res.send(bookmarks)
    } catch (err) {
      res.status(500).send({
        error: 'An error was occured when try to fetch songs'
      })
    }
  },

  async post (req, res) {
    try {
      const userId = req.user.id

      const {songId} = req.body
      const bookmark = await Bookmarks.findOne({
        where: {
          SongId: songId,
          UserId: userId
        }
      })
      if (bookmark) {
        return res.status(400).send({
          error: 'you already have this set as a bookmark'
        })
      }
      const newBookmark = await Bookmarks.create({
        SongId: songId,
        UserId: userId
      })

      res.send(newBookmark)
    } catch (err) {
      console.log(err)
      res.status(500).send({
        error: 'an error has occured trying to create the bookmark'
      })
    }
  },

  async delete (req, res) {
    try {
      console.log('--> ' + req.user.id)
      const userId = req.user.id
      const {bookmarkId} = req.params

      const bookmark = await Bookmarks.findOne({
        where: {
          id: bookmarkId,
          UserId: userId
        }
      })
      if (!bookmark) {
        return res.status(403).send({
          error: 'You do not have to access to this bookmark'
        })
      }
      await bookmark.destroy()

      res.send(bookmark)
    } catch (err) {
      res.status(500).send({
        error: 'An error was occured when try to delete bookmark'
      })
    }
  }

}
