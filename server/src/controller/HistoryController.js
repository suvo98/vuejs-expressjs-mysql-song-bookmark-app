const {
  History,
  Song
} = require('../models')
const _ = require('lodash')
module.exports = {
  async index (req, res) {
    try {
      const userId = req.user.id
      const {songId} = req.query
      console.log('-----------------<br>')
      console.log('-----------------<br>')
      console.log('-----------------<br>')
      console.log(userId)
      console.log('-----------------<br>')
      console.log('-----------------<br>')
      const where = {
        UserId: userId
      }
      if (songId) {
        where.SongId = songId
      }
      const histories = await History.findAll({
        where: where,
        include: [
          {
            model: Song
          }
        ]
      })
        .map(history => history.toJSON())
        .map(history => _.extend(
          {},
          history.Song,
          history
        ))

      res.send(_.uniqBy(histories, history => history.SongId))
    } catch (err) {
      res.status(500).send({
        error: 'An error was occured when try to fetch history'
      })
    }
  },

  async post (req, res) {
    try {
      const {songId, userId} = req.body

      const history = await History.create({
        SongId: songId,
        UserId: userId
      })

      res.send(history)
    } catch (err) {
      console.log(err)
      res.status(500).send({
        error: 'an error has occured trying to create the songs histroy'
      })
    }
  }

}
