const AuthenticationController = require('./controller/AuthenticationController')

const SongsController = require('./controller/SongsController')
const BookmarksController = require('./controller/BookmarksController')
const HistoryController = require('./controller/HistoryController')

const AuthenticationControllerPolicy = require('./policies/AuthenticationControllerPolicy')
const isAuthenticated = require('./policies/isAuthenticated')


module.exports = (app) => {
  app.post('/register',
    AuthenticationControllerPolicy.register,
    AuthenticationController.register
  )

  app.post('/login',
    AuthenticationController.login
  )

  app.get('/songs',
    SongsController.index
  )

  app.get('/songs/:songId',
    SongsController.show
  )

  app.post('/song',
    SongsController.post
  )

  app.put('/songs/:songId',
    SongsController.put
  )

  app.get('/bookmarks',
    isAuthenticated,
    BookmarksController.index
  )

  app.post('/bookmarks',
    isAuthenticated,
    BookmarksController.post
  )

  app.delete('/bookmarks/:bookmarkId',
    isAuthenticated,
    BookmarksController.delete
  )

  app.get('/histories',
    isAuthenticated,
    HistoryController.index
  )

  app.post('/history',
    HistoryController.post
  )
}
