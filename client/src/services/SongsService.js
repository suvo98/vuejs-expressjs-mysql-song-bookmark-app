import Api from '@/services/Api'

export default {
  index (searchValue) {
    return Api().get('songs', {
      params: {
        search: searchValue
      }
    })
  },
  show (songId) {
    return Api().get(`songs/${songId}`)
  },
  post (song) {
    return Api().post('song', song)
  },
  put (song) {
    return Api().put(`songs/${song.id}`, song)
  }
}
