import Api from '@/services/Api'

export default {
  index (bookmarks) {
    return Api().get('bookmarks', {
      params: bookmarks
    })
  },
  post (bookmark) {
    return Api().post('bookmarks', bookmark)
  },
  delete (bookmarkId) {
    return Api().delete(`bookmarks/${bookmarkId}`)
  }
}
