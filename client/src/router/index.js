import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Register from '@/components/Register'
import Login from '@/components/Login'
import Songs from '@/components/Songs/SongsList'
import SongsCreate from '@/components/Songs/SongsCreate'
import SongsEdit from '@/components/Songs/SongsEdit'
import ViewSong from '@/components/Songs/View/SongView'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/root',
      name: 'root',
      component: HelloWorld
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/songs',
      name: 'Songs',
      component: Songs
    },
    {
      path: '/songs/create',
      name: 'Songs-Create',
      component: SongsCreate
    },
    {
      path: '/songs/:songId/edit',
      name: 'Songs-Edit',
      component: SongsEdit
    },
    {
      path: '/songs/:songId',
      name: 'Song',
      component: ViewSong
    },
    {
      path: '*',
      redirect: 'Songs'
    }

  ]
})
